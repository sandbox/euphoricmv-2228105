<?php

/**
 * @file
 */

namespace Drupal\btn_authorship\Controller;

use Drupal\Core\Controller\ControllerBase;

class authorshipController extends ControllerBase {

  function confirmation_page() {
    return btn_authorship_confirmation();
  }

}