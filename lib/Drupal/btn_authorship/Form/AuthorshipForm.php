<?php

/**
 * @file
 * Contains \Drupal\btn_authorship\Form\AuthorshipForm.
 */
namespace Drupal\btn_authorship\Form;

use Drupal\Core\Form\FormInterface;
use Drupal\btn_authorship\Storage\AuthorshipStorage;

/**
 * Implements an example form.
 */
class AuthorshipForm implements FormInterface {
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'btn_authorship_form';
  }

  public function buildForm(array $form, array &$form_state) {
    $form = array();

    $form['#prefix'] = '<h2>' . t('Yes, I want to be author') . '</h2>';

    $form['firstname'] = array(
      '#type' => 'textfield',
      '#title' => t('First name'),
      '#required' => TRUE,
      '#prefix' => '<div class="inline-name">',
    );

    $form['middlename'] = array(
      '#type' => 'textfield',
      '#title' => t('Middle name'),
      '#size' => 5,
      '#maxlength'=> 5,
      '#required' => TRUE,
    );

    $form['lastname'] = array(
      '#type' => 'textfield',
      '#title' => t('Last name'),
      '#required' => TRUE,
      '#suffix' => '</div>',
    );

    $form['gender'] = array(
      '#type' => 'radios',
      '#title' => t('Gender'),
      '#options' => array(
        t('Male') => t('Male'),
        t('Female') => t('Female'),
      ),
//      '#required' => TRUE,
    );

    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
//      '#required' => TRUE,
    );


    $form['phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone number'),
//      '#required' => TRUE,
    );

    $form['work'] = array(
      '#type' => 'textfield',
      '#title' => t('Work/Business'),
//      '#required' => TRUE,
    );

    $form['motivation'] = array(
      '#type' => 'textfield',
      '#title' => t('Motivation'),
//      '#required' => TRUE,
    );

    $form['max_authorship'] = array(
      '#type' => 'hidden',
      '#value' => \Drupal::config('btn_authorship.settings')->get('max_authors'),
    );


    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
//    if (!valid_email_address($form_state['values']['mail'])) {
//      $this->setFormError('mail', $form_state, t('You must enter a valid e-mail address.'));
//    }
//    if (!preg_match('/^([0-9\(\)\/\+ \-]*)$/',$form_state['values']['phone'])) {
//      $this->setFormError('phone', $form_state, t('Not valid phone number.'));
//    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $firstname = $form_state['values']['firstname'];
    $lastname = $form_state['values']['lastname'];

    AuthorshipStorage::add($firstname, $lastname);
//    $form_state['redirect'] = 'authorship/confirmation';
//    drupal_set_message($this->t('Your phone number is @number', array('@number' => $form_state['values']['phone'])));
  }
}


