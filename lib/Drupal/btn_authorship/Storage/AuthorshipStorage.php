<?php
namespace Drupal\btn_authorship\Storage;

class AuthorshipStorage {

  // Returns all participant names stored in database.
  static function getAll() {
    return db_query('SELECT name FROM {btn_authorship}')->fetchCol();
  }

  /*
   * Stores participant name in database.
   *
   * @param $name
   *   participant name.
   */
  static function add($firstname, $lastname) {
    $request = \Drupal::request();
    db_insert('btn_authorship')->fields(array('firstname' => $firstname, 'lastname' => $lastname))->execute();
  }

  /**
   * Deletes participant name in database.
   *
   * @param $name
   *   participant name.
   */
  static function delete($name) {
    db_delete('btn_authorship')->condition('name', $name)->execute();
  }
}